==========================
Format Specification Suite
==========================

:format: DATEX II Elaborated Data Publication - Traffic Status
:uri: cz-ndic_d2-traffic-status-v1.1

This repository provides tools and files for given format:

- schema
- sample(s)
- documentation
- test suite
- unified `tox` based interface for related tools

About described format
======================

For all details, see `FORMAT.yaml`.

Using provided tools
====================

For all details, see `tox.rst`.

Changelog
=========

version 1.0.0 

- Innitial format and documentation

version 1.0.1

- revision of the documentation, proofreading, changes to examples


version 1.1.0

- added feedtype to schema and samples
- added sourceIdentification to samples